package business.bean;

import struts.bean.ActionForm;

public class PanduanForm  extends ActionForm{
	private String name;
	private String pwd;
	 
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	
	
}
