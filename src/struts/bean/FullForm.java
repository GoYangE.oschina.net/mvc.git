package struts.bean;

import java.lang.reflect.Field;

import javax.servlet.http.HttpServletRequest;

public class FullForm {

	public static ActionForm full(String formPath,HttpServletRequest request){
		ActionForm af=null;
		try {
			Class cla=Class.forName(formPath);
			af=(ActionForm) cla.newInstance();
			Field[] fieldArr=cla.getDeclaredFields();
			for(Field f:fieldArr){
				f.setAccessible(true);
				f.set(af, request.getParameter(f.getName()));
				f.setAccessible(false);
			}
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return af; 
	}
	
	
}
