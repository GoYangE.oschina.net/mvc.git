package struts.controller;

import java.io.IOException;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import struts.bean.ActionForm;
import struts.bean.FullForm;
import struts.bean.XmlBean;
import struts.interfaces.Action;
import struts.utils.StrutsConfigUtil;

/**
 * Servlet implementation class StrutsServlet
 */
@WebServlet("/StrutsServlet")
public class StrutsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public StrutsServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//System.out.println("name="+request.getParameter("name"));
		//System.out.println(request.getServletPath());
		//System.out.println(StrutsConfigUtil.getUrl(request.getServletPath()));
		String path=StrutsConfigUtil.getUrl(request.getServletPath());
		Map<String,XmlBean> map=(Map<String, XmlBean>) this.getServletContext().getAttribute("struts");
		XmlBean xb=map.get(path);
		String formClass=xb.getFormClass();
		ActionForm form=FullForm.full(formClass, request);
		String actionType=xb.getActionType();
		Action action=null;
		String url = "";
		try {
			Class clas=Class.forName(actionType); 
			action = (Action) clas.newInstance();
			url = action.execute(request,response, form, xb.getActionForward());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("�������쳣");
		}
		RequestDispatcher dis=request.getRequestDispatcher(url);
		dis.forward(request, response);
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
