package struts.controller;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.jdom.JDOMException;

import struts.bean.XmlBean;
import struts.utils.StrutsConfigUtil;

/**
 * Application Lifecycle Listener implementation class ActionListener
 *
 */
@WebListener
public class ActionListener implements ServletContextListener {

    /**
     * Default constructor. 
     */
    public ActionListener() {
        // TODO Auto-generated constructor stub
    }

	/**
     * @see ServletContextListener#contextDestroyed(ServletContextEvent)
     */
    public void contextDestroyed(ServletContextEvent arg0)  { 
        System.out.println("系统已注销");
    }

	/**
     * @see ServletContextListener#contextInitialized(ServletContextEvent)
     */
    public void contextInitialized(ServletContextEvent arg0)  { 
         ServletContext context=arg0.getServletContext();
    	 String xmlPath=context.getInitParameter("struts-config");
         String tomcatPath=context.getRealPath("\\");
         try {
			Map<String,XmlBean> map=StrutsConfigUtil.loadStrutsConfig(tomcatPath+xmlPath);
			context.setAttribute("struts", map);
         } catch (JDOMException e) {
			// TODO Auto-generated catch block 
			e.printStackTrace();
		 } catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		 } 
         System.out.println("信息：系统加载完成");
    
    }
	
}
